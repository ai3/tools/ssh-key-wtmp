package main

import "testing"

func TestMain_Reboot(t *testing.T) {
	withTestDB(t, func() {
		withTestAuthorizedKeys(t, testAuthorizedKeys, func(_ string) {
			withTestEnv(map[string]string{
				"PAM_TYPE":        "open_session",
				"PAM_USER":        "root",
				"SSH_CONNECTION":  "127.0.0.1 10023",
				"SSH_AUTH_INFO_0": "publickey ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKHGU3+iLYiepGfenTlieDiv4hY4r0PlZa+mnb7NoKkc",
			}, func() {
				if err := run(0); err != nil {
					t.Fatal(err)
				}
			})

			sessions, err := query("")
			if err != nil {
				t.Fatalf("query: %v", err)
			}
			if len(sessions) != 1 {
				t.Fatalf("query: sessions=%+v", sessions)
			}

			if err := cleanupAfterReboot(1); err != nil {
				t.Fatalf("cleanupAfterReboot: %v", err)
			}

			// The session should be no longer active.
			sessions, err = query("")
			if err != nil {
				t.Fatalf("query: %v", err)
			}
			if len(sessions) != 1 {
				t.Fatalf("query: sessions=%+v", sessions)
			}
			if sessions[0].EndedAt.IsZero() || !sessions[0].EndedReboot {
				t.Fatalf("session was not properly finalized: %+v", sessions[0])
			}
		})
	})
}

func TestMain_StartStop(t *testing.T) {
	withTestDB(t, func() {
		withTestAuthorizedKeys(t, testAuthorizedKeys, func(_ string) {
			withTestEnv(map[string]string{
				"PAM_TYPE":        "open_session",
				"PAM_USER":        "root",
				"SSH_CONNECTION":  "127.0.0.1 10023",
				"SSH_AUTH_INFO_0": "publickey ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKHGU3+iLYiepGfenTlieDiv4hY4r0PlZa+mnb7NoKkc",
			}, func() {
				if err := run(0); err != nil {
					t.Fatalf("open_session: %v", err)
				}
			})

			withTestEnv(map[string]string{
				"PAM_TYPE":       "close_session",
				"SSH_CONNECTION": "127.0.0.1 10023",
			}, func() {
				if err := run(0); err != nil {
					t.Fatalf("close_session: %v", err)
				}
			})

			sessions, err := query("")
			if err != nil {
				t.Fatalf("query: %v", err)
			}
			if len(sessions) != 1 {
				t.Fatalf("query: sessions=%+v", sessions)
			}
			if sessions[0].isActive(0) {
				t.Fatalf("closed session is still active: %+v", sessions[0])
			}
		})
	})
}
