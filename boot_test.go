package main

import "testing"

func TestGetBootTime(t *testing.T) {
	b, err := getBootTime()
	if err != nil {
		t.Fatal(err)
	}
	if b <= 0 {
		t.Fatalf("btime <= 0: %v", b)
	}
}
