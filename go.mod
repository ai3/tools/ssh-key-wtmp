module git.autistici.org/ai3/tools/ssh-key-wtmp

go 1.21.0

toolchain go1.23.2

require (
	git.autistici.org/ai3/go-common v0.0.0-20241017171051-880a2c5ae7f4
	github.com/oschwald/maxminddb-golang v1.13.0
	golang.org/x/crypto v0.28.0
)

require (
	github.com/mattn/go-sqlite3 v1.14.23 // indirect
	golang.org/x/sys v0.26.0 // indirect
)
