package sqlutil

import (
	"database/sql"
	"strings"
)

// QueryBuilder is a very simple programmatic query builder, to
// simplify the operation of adding WHERE and ORDER BY clauses
// programatically.
type QueryBuilder struct {
	base  string
	tail  string
	where []string
	args  []interface{}
}

// NewQuery returns a query builder starting with the given base query.
func NewQuery(s string) *QueryBuilder {
	return &QueryBuilder{base: s}
}

// OrderBy adds an ORDER BY clause.
func (q *QueryBuilder) OrderBy(s string) *QueryBuilder {
	q.tail += " ORDER BY "
	q.tail += s
	return q
}

// Where adds a WHERE clause with associated argument(s).
func (q *QueryBuilder) Where(clause string, args ...interface{}) *QueryBuilder {
	q.where = append(q.where, clause)
	q.args = append(q.args, args...)
	return q
}

// Query executes the resulting query in the given transaction.
func (q *QueryBuilder) Query(tx *sql.Tx) (*sql.Rows, error) {
	s := q.base
	if len(q.where) > 0 {
		s += " WHERE "
		s += strings.Join(q.where, " AND ")
	}
	s += q.tail

	return tx.Query(s, q.args...)
}
