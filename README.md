# ssh-key-wtmp

This tool is meant to complement the
[ai3/float](https://git.autistici.org/ai3/float) SSH authentication
model by providing a meaningful SSH access log, and ultimately a
*wtmp* analog that works with real admin identities.

The problem is that float's model of "root access with admin SSH keys"
does not result in useful logs: SSH logs the key fingerprint thanks to
the `LogLevel VERBOSE` directive, but mapping those back to users is a
manual, complex process (among other things, SSH tooling doesn't
exactly make it easy to go from a public key string to a fingerprint).

The solution to this is a mechanism by which, on every successful SSH
connection, we look up the key used, map it to an admin username via
the authorized_keys *comment* field, and create an additional syslog
entry with those.

The implementation relies on PAM, taking advantage of
[pam_exec](https://linux.die.net/man/8/pam_exec) to run a small
logging command when a successful SSH connection is
established. Hooking this up to the PAM *session* stage allows us to
detect begin and end of the sessions (by looking at PAM_TYPE being
*open_session* or *close_session*).

The major feature of the tool is logging over syslog the connection
details, so that they will eventually be transfered to the centralized
logging system. But it is also possible to query the local session
database just as one would with the *last* tool: just invoking
*ssh-key-wtmp* will list the last 100 sessions on the local host.

The tool handles reboots by starting the ssh-key-wtmp-boot.service
systemd unit at boot, which looks through the wtmp database and emits
*close\_session* logs for the sessions that were active at reboot
time. This ensures that all logged sessions have a matching
*open\_session*/*close\_session* pair.

## Configuration

The tool needs to retrieve the specific SSH key, that was used for
authentication, from an authorized\_keys file in order to retrieve its
comment. It is important that the *--authorized-keys-file*
command-line option to ssh-key-wtmp matches the *AuthorizedKeysFile*
directive in your sshd\_config. Note that the default value for this
option works for the ai3/float environment, but it is not the SSH
default. For that, you'll need to set:

```
--authorized-keys-file=".ssh/authorized_keys .ssh/authorized_keys2"
```

### PAM

Something like the following, in */etc/pam.d/sshd*, should be
sufficient for system integration:

```
session [default=ignore] pam_exec.so quiet seteuid /usr/bin/ssh-key-wtmp
```

The Debian package will set this up automatically in
*/etc/pam.d/common-session*, which is slightly sub-optimal because it
will get invoked (harmlessly) from other services such as cron. You
might be better off manually editing */etc/pam.d/sshd* directly.

It is important that this pam_exec line is placed **after**
pam_systemd, as ssh-key-wtmp needs variables defined by it.


### Location data

By default the tool will log the IP address of the client connection,
and it will augment it with GeoIP data to simplify forensic analysis,
should it be necessary. It will also attempt to detect connections
using Tor exit nodes, and track those separately to avoid polluting
location data with meaningless values.

This task is performed using MaxMind GeoIP databases, and the current
up-to-date list of Tor exit nodes. The location of these files can be
customized with the *--tor-exits* and *--maxminddb* command-line
options.

The exact amount of data that will be logged can be controlled,
partially or fully disabled, using various command-line options such
as *--location-disable-ip*, *--location-disable-city*, etc. (use
*--help* to see the full list of individual fields that can be
controlled).
