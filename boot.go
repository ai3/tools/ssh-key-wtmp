package main

import (
	"bufio"
	"bytes"
	"errors"
	"os"
	"strconv"
)

var btimePfx = []byte("btime ")

func getBootTime() (int64, error) {
	f, err := os.Open("/proc/stat")
	if err != nil {
		return 0, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Bytes()
		if bytes.HasPrefix(line, btimePfx) {
			return strconv.ParseInt(string(line[len(btimePfx):]), 10, 64)
		}
	}
	return 0, errors.New("btime not found in /proc/stat")
}
