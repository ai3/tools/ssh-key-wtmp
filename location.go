package main

import (
	"bufio"
	"errors"
	"flag"
	"net"
	"os"
	"strings"

	"github.com/oschwald/maxminddb-golang"
)

const torCountry = "Tor"

var errNotAnIP = errors.New("not an IP address")

// Default location of MaxMind GeoIP databases on Debian.
var defaultGeoIPPaths = []string{
	"/var/lib/GeoIP/GeoLite2-City.mmdb",
	"/var/lib/GeoIP/GeoLite2-Country.mmdb",
	"/var/lib/GeoIP/GeoLite2-ASN.mmdb",
}

// Multiple-valued command-line flag.
type listFlag []string

func (l listFlag) String() string {
	return strings.Join(l, ",")
}

func (l *listFlag) Set(value string) error {
	*l = append(*l, value)
	return nil
}

// Configuration for the location subsystem. This covers many options,
// we may want to replace it with a more compact and user-friendly
// representation (keywords?).
type locationConfig struct {
	geoIPPaths     listFlag
	torExits       string
	disableIP      bool
	disableCity    bool
	disableCountry bool
	disableASN     bool
}

func (c *locationConfig) SetFlags(f *flag.FlagSet) {
	f.BoolVar(&c.disableIP, "location-disable-ip", false, "disable logging of IP addresses")
	f.BoolVar(&c.disableCity, "location-disable-city", false, "disable logging of City-level information")
	f.BoolVar(&c.disableCity, "location-disable-country", false, "disable logging of Country-level information")
	f.BoolVar(&c.disableCity, "location-disable-asn", false, "disable logging of ASN-level information")
	f.StringVar(&c.torExits, "tor-exits", "/var/lib/tor-exits/exits", "`path` to the list of Tor exit nodes")
	f.Var(&c.geoIPPaths, "maxminddb", "additional `path` to a MaxMindDB GeoLite2 database, can be specified multiple times")
}

// Global configuration object, initialized and connected to
// command-line flags via init().
var kLocationConfig *locationConfig

func init() {
	kLocationConfig = &locationConfig{
		geoIPPaths: defaultGeoIPPaths,
	}
	kLocationConfig.SetFlags(flag.CommandLine)
}

// Struct used to lookup all GeoIP records we know about.
type geoIPRecord struct {
	Country struct {
		ISOCode string `maxminddb:"iso_code"`
	} `maxminddb:"country"`
	City struct {
		Names struct {
			En string `maxminddb:"en"`
		} `maxminddb:"names"`
	} `maxminddb:"city"`
	ASN struct {
		Number       string `maxminddb:"autonomous_system_number"`
		Organization string `maxminddb:"autonomous_system_organization"`
	} `maxminddb:"asn"`
}

// Perform a lookup for 'ip' in a MaxMind GeoIP database, and update
// 'loc' with the results (compatibly with the constraints specified
// in the configuration).
func geoLookup(c *locationConfig, dbPath string, ip net.IP, loc *location) error {
	geodb, err := maxminddb.Open(dbPath)
	if err != nil {
		return err
	}
	defer geodb.Close()

	var record geoIPRecord
	if err := geodb.Lookup(ip, &record); err != nil {
		return err
	}

	if !c.disableCity && record.City.Names.En != "" {
		loc.City = record.City.Names.En
	}
	if !c.disableCountry && record.Country.ISOCode != "" {
		loc.Country = record.Country.ISOCode
	}
	if !c.disableASN {
		if record.ASN.Number != "" {
			loc.ASN = record.ASN.Number
		}
		if record.ASN.Organization != "" {
			loc.ASNOrganization = record.ASN.Organization
		}
	}
	return nil
}

// Perform a lookup for 'ip' in multiple MaxMind GeoIP databases, and
// aggregate the results in 'loc'. This function silently ignores all
// errors (arguably it might log warnings, except for 'file not found').
func multiGeoLookup(c *locationConfig, ip net.IP, loc *location) error {
	for _, dbPath := range c.geoIPPaths {
		_ = geoLookup(c, dbPath, ip, loc)
	}
	return nil
}

// Returns true if the given IP appears in tor-exits. We need to parse
// entries in the file due to variance in IPv6 representation. There
// are definitely faster ways to go about it.
func torExitLookup(c *locationConfig, ip net.IP) (bool, error) {
	f, err := os.Open(c.torExits)
	if err != nil {
		return false, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		curIP := net.ParseIP(scanner.Text())
		if ip.Equal(curIP) {
			return true, nil
		}
	}
	return false, scanner.Err()
}

// Fill in 'loc' with all the location data we can / want to collect about 'addr'.
func augmentLocation(c *locationConfig, addr string, loc *location) error {
	ip := net.ParseIP(addr)
	if ip == nil {
		return errNotAnIP
	}

	if !c.disableIP {
		loc.IP = addr
	}

	// Tor is mapped to a Country.
	if c.torExits != "" {
		if ok, _ := torExitLookup(c, ip); ok {
			loc.Country = torCountry
			return nil
		}
	}

	return multiGeoLookup(c, ip, loc)
}
